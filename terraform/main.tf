module "k8s-network" {
  source           = "./modules/k8s-network"
  vpc_network_main = var.vpc_network_main
  subnet_name      = var.subnet_name
  subnet_cidr      = var.subnet_cidr
  subnet_zone      = var.subnet_zone
  k8s_white_ips    = var.k8s_white_ips
}

module "k8s-cluster" {
  source                    = "./modules/k8s-cluster"
  k8s_version               = var.k8s_version
  network_id                = module.k8s-network.network_id
  sa_name                   = var.sa_name
  sa_alb_name               = var.sa_alb_name
  folder_id                 = var.folder_id
  security_group_k8s_master = module.k8s-network.security_group_k8s_master
  security_group_internal   = module.k8s-network.security_group_internal
  security_group_k8s_worker = module.k8s-network.security_group_k8s_worker
  subnet_id                 = module.k8s-network.subnet_id
  subnet_zone               = var.subnet_zone
  cluster_name              = var.cluster_name
  node_memory               = var.node_memory
  node_cpu                  = var.node_cpu
  node_scale_size           = var.node_scale_size
  node_disk                 = var.node_disk
}