terraform {
  required_providers {
    yandex = {
      source = "terraform-registry.storage.yandexcloud.net/yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.87.0"

  backend "s3" {
    endpoint = "storage.yandexcloud.net"
    bucket   = "momo-tfstate"
    region   = "ru-central1"
    key      = "tfstate/momo.tfstate"

    skip_region_validation      = true
    skip_credentials_validation = true
  }

}