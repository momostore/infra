#===========ROOT===============
#==provider==
variable "token" {
  description = "Yandex Cloud oauth token "
  type        = string
}

variable "cloud_id" {
  description = "The cloud ID"
  type        = string
}

variable "folder_id" {
  description = "The folder ID"
  type        = string
}

variable "default_zone" {
  description = "Default zone for k8s"
}

#===========K8S-NETWORK==================
#==vpc.tf==

variable "vpc_network_main" {
  description = "Main network fo k8s cluster"
  type        = string
}

variable "subnet_name" {
  description = "Subnet name"
  type        = string
}

variable "subnet_zone" {
  description = "Subnet zone"
  type        = string
}

variable "subnet_cidr" {
  description = "Subnet cidr"
  type        = string
}

variable "k8s_white_ips" {
  description = "White IPs for k8s api"
}

#===========K8S-CLUSTER==================
#==cluster.tf==
variable "k8s_version" {
  description = "k8s cluster version"
}

variable "region" {

}

variable "cluster_name" {
  type = string
}

#==service_account.tf==
variable "sa_name" {
  description = "Name of service account"
}
variable "sa_alb_name" {
  description = "Name of sa for alb"
}
#==node_group.tf==

variable "node_memory" {
  description = "Memory amount for each node"
}

variable "node_cpu" {
  description = "CPU amount for each node"
}

variable "node_scale_size" {
  description = "Counts of nodes"
}

variable "node_disk" {
  description = "Boot disk size"
}