output "network_id" {
  value = module.k8s-network.network_id
}

output "subnet_id" {
  value = module.k8s-network.subnet_id
}