resource "yandex_kubernetes_node_group" "k8s-nodes" {
  cluster_id  = yandex_kubernetes_cluster.k8s-cluster.id
  name        = "node-group-a"
  description = "Worker in zona a"
  version     = var.k8s_version
  instance_template {
    platform_id = "standard-v1"
    network_interface {
      nat                = true
      subnet_ids         = [var.subnet_id]
      security_group_ids = [var.security_group_k8s_worker, var.security_group_internal]
    }
    resources {
      memory = var.node_memory
      cores  = var.node_cpu
    }
    boot_disk {
      type = "network-hdd"
      size = var.node_disk
    }
    scheduling_policy {
      preemptible = false
    }
  }
  scale_policy {
    # auto_scale {
    #   min     = 1
    #   max     = 2
    #   initial = 1
    # }
    fixed_scale {
      size = var.node_scale_size
    }
  }
  allocation_policy {
    location {
      zone = var.subnet_zone
    }
  }
}
