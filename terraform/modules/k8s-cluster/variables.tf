#===========K8S-CLUSTER==================
#==cluster.tf==
variable "network_id" {
  description = "Main network ID"
}

variable "k8s_version" {
  description = "k8s cluster version"
}

variable "subnet_zone" {
  description = "Subnet zone"
  type        = string
}

variable "subnet_id" {
  description = "Subnet zone id"
  type        = string
}

variable "security_group_k8s_master" {
  description = "ID of k8s security groups"
}

variable "security_group_internal" {
  description = "ID of k8s security groups"
}

variable "cluster_name" {
  type = string
}

#==service_account.tf==
variable "sa_name" {
  description = "Name of service account"
}

variable "sa_alb_name" {
  description = "Name of sa for alb"
  
}

variable "folder_id" {

}

#==node_group.tf==
variable "security_group_k8s_worker" {

}

variable "node_memory" {
  description = "Memory amount for each node"
}

variable "node_cpu" {
  description = "CPU amount for each node"
}

variable "node_scale_size" {
  description = "Counts of nodes"
}

variable "node_disk" {
  description = "Boot disk size"
}