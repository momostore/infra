resource "yandex_kubernetes_cluster" "k8s-cluster" {
  name       = var.cluster_name
  network_id = var.network_id
  master {
    version   = var.k8s_version
    public_ip = true
    zonal {
      zone      = var.subnet_zone
      subnet_id = var.subnet_id
    }
    security_group_ids = [var.security_group_k8s_master, var.security_group_internal]
  }
  service_account_id      = yandex_iam_service_account.k8s-account.id
  node_service_account_id = yandex_iam_service_account.k8s-account.id
  depends_on = [
    yandex_resourcemanager_folder_iam_binding.editor,
    yandex_resourcemanager_folder_iam_binding.images-puller,
  ]
  kms_provider {
    key_id = yandex_kms_symmetric_key.kms-key.id
  }
}