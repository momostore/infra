#===========K8S-NETWORK==================
#==vpc.tf==
variable "vpc_network_main" {
  description = "Main network fo k8s cluster"
  type        = string
}

variable "subnet_name" {
  description = "Subnet name"
  type        = string
}

variable "subnet_zone" {
  description = "Subnet zone"
  type        = string
}

variable "subnet_cidr" {
  description = "Subnet cidr"
  type        = string
}

variable "k8s_white_ips" {
  description = "White IPs for k8s api"
}
