# Network
Модуль создаёт следующие ресурсы:

resource "yandex_vpc_network" "k8s_network"
Основная сеть кластера K8s

resource "yandex_vpc_subnet" "k8s-subnet"
Зону кластера

resource "yandex_vpc_security_group" "k8s-public-services"
Группу безопасности нашего кластера