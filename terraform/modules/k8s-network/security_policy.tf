resource "yandex_vpc_security_group" "internal" {
  name        = "internal"
  description = "Разрешает весь трафик в указанных подсетях. Применять и к кластеру и к нодам"
  network_id  = yandex_vpc_network.k8s_network.id
  labels = {
    firewall = "yc_internal"
  }
  ingress {
    protocol       = "ANY"
    description    = "self"
    v4_cidr_blocks = [var.subnet_cidr]
    from_port      = 0
    to_port        = 65535
  }
  egress {
    protocol       = "ANY"
    description    = "self"
    v4_cidr_blocks = [var.subnet_cidr]
    from_port      = 0
    to_port        = 65535
  }
  ingress {
    protocol          = "TCP"
    description       = "access to api k8s from Yandex lb"
    predefined_target = "loadbalancer_healthchecks"
    from_port         = 0
    to_port           = 65535
  }
  ingress {
    protocol       = "ICMP"
    description    = "Правило разрешает отладочные ICMP-пакеты из внутренних подсетей."
    v4_cidr_blocks = ["172.16.0.0/12", "10.0.0.0/8", "192.168.0.0/16"]
  }
}

resource "yandex_vpc_security_group" "k8s_master" {
  name        = "k8s-master"
  description = "Managed by terraform"
  network_id  = yandex_vpc_network.k8s_network.id
  labels = {
    firewall = "k8s-master"
  }
  ingress {
    protocol       = "TCP"
    description    = "access to api k8s"
    v4_cidr_blocks = [var.k8s_white_ips]
    port           = 443
  }
  ingress {
    protocol       = "TCP"
    description    = "access to api k8s"
    v4_cidr_blocks = [var.k8s_white_ips]
    port           = 6443
  }
  egress {
    protocol       = "ANY"
    description    = "any connections"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 0
    to_port        = 65535
  }
}

resource "yandex_vpc_security_group" "k8s_worker" {
  name        = "k8s-worker"
  description = "Managed by terraform"
  network_id  = yandex_vpc_network.k8s_network.id
  labels = {
    firewall = "k8s-worker"
  }
  ingress {
    protocol       = "ANY"
    description    = "any connections"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 0
    to_port        = 65535
  }
  egress {
    protocol       = "ANY"
    description    = "any connections"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = 0
    to_port        = 65535
  }
}