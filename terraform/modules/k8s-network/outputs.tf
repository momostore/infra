output "network_id" {
  value = yandex_vpc_network.k8s_network.id
}

output "security_group_k8s_master" {
  value = yandex_vpc_security_group.k8s_master.id
}

output "security_group_internal" {
  value = yandex_vpc_security_group.internal.id

}

output "security_group_k8s_worker" {
  value = yandex_vpc_security_group.k8s_worker.id

}

output "subnet_id" {
  value = yandex_vpc_subnet.k8s-subnet.id
}