resource "yandex_vpc_network" "k8s_network" {
  name = var.vpc_network_main
}

resource "yandex_vpc_subnet" "k8s-subnet" {
  name           = var.subnet_name
  v4_cidr_blocks = [var.subnet_cidr]
  zone           = var.subnet_zone
  network_id     = yandex_vpc_network.k8s_network.id
}
